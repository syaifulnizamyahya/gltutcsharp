﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// http://www.arcsynthesis.org/gltut/Positioning/Tut04%20The%20Matrix%20Has%20You.html
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2004%20Objects%20at%20Rest/MatrixPerspective.cpp?at=default

using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using framework;

namespace MatrixPerspective
{
	class Program : GameWindow
	{
		private int theProgram;

		private int offsetUniform;
		private int perspectiveMatrixUnif;

		void InitializeProgram()
		{
			var shaderList = new List<int>();

			shaderList.Add(Framework.LoadShader(ShaderType.VertexShader, @"data\MatrixPerspective.vert"));
			shaderList.Add(Framework.LoadShader(ShaderType.FragmentShader, @"data\StandardColors.frag"));

			theProgram = Framework.CreateProgram(shaderList);

			offsetUniform = GL.GetUniformLocation(theProgram, "offset");

			perspectiveMatrixUnif = GL.GetUniformLocation(theProgram, "perspectiveMatrix");

			float fFrustumScale = 1.0f;
			float fzNear = 0.5f;
			float fzFar = 3.0f;

			var theMatrix = new Matrix4();
			theMatrix = Matrix4.Zero;

			theMatrix.M11 = fFrustumScale;
			theMatrix.M22 = fFrustumScale;
			theMatrix.M33 = (fzFar + fzNear)/(fzNear - fzFar);
			theMatrix.M43 = (2*fzFar*fzNear)/(fzNear - fzFar);
			theMatrix.M34 = -1.0f;
			
			GL.UseProgram(theProgram);
			GL.UniformMatrix4(perspectiveMatrixUnif, 1, false, ref theMatrix.Row0.X);
			GL.UseProgram(0);
		}

		readonly Vector4[] vertexData =
		{
			new Vector4( 0.25f,  0.25f, -1.25f, 1.0f),
			new Vector4( 0.25f, -0.25f, -1.25f, 1.0f),
			new Vector4(-0.25f,  0.25f, -1.25f, 1.0f),

			new Vector4( 0.25f, -0.25f, -1.25f, 1.0f),
			new Vector4(-0.25f, -0.25f, -1.25f, 1.0f),
			new Vector4(-0.25f,  0.25f, -1.25f, 1.0f),

			new Vector4( 0.25f,  0.25f, -2.75f, 1.0f),
			new Vector4(-0.25f,  0.25f, -2.75f, 1.0f),
			new Vector4( 0.25f, -0.25f, -2.75f, 1.0f),

			new Vector4( 0.25f, -0.25f, -2.75f, 1.0f),
			new Vector4(-0.25f,  0.25f, -2.75f, 1.0f),
			new Vector4(-0.25f, -0.25f, -2.75f, 1.0f),

			new Vector4(-0.25f,  0.25f, -1.25f, 1.0f),
			new Vector4(-0.25f, -0.25f, -1.25f, 1.0f),
			new Vector4(-0.25f, -0.25f, -2.75f, 1.0f),

			new Vector4(-0.25f,  0.25f, -1.25f, 1.0f),
			new Vector4(-0.25f, -0.25f, -2.75f, 1.0f),
			new Vector4(-0.25f,  0.25f, -2.75f, 1.0f),

			new Vector4( 0.25f,  0.25f, -1.25f, 1.0f),
			new Vector4( 0.25f, -0.25f, -2.75f, 1.0f),
			new Vector4( 0.25f, -0.25f, -1.25f, 1.0f),

			new Vector4( 0.25f,  0.25f, -1.25f, 1.0f),
			new Vector4( 0.25f,  0.25f, -2.75f, 1.0f),
			new Vector4( 0.25f, -0.25f, -2.75f, 1.0f),

			new Vector4( 0.25f,  0.25f, -2.75f, 1.0f),
			new Vector4( 0.25f,  0.25f, -1.25f, 1.0f),
			new Vector4(-0.25f,  0.25f, -1.25f, 1.0f),

			new Vector4( 0.25f,  0.25f, -2.75f, 1.0f),
			new Vector4(-0.25f,  0.25f, -1.25f, 1.0f),
			new Vector4(-0.25f,  0.25f, -2.75f, 1.0f),

			new Vector4( 0.25f, -0.25f, -2.75f, 1.0f),
			new Vector4(-0.25f, -0.25f, -1.25f, 1.0f),
			new Vector4( 0.25f, -0.25f, -1.25f, 1.0f),

			new Vector4( 0.25f, -0.25f, -2.75f, 1.0f),
			new Vector4(-0.25f, -0.25f, -2.75f, 1.0f),
			new Vector4(-0.25f, -0.25f, -1.25f, 1.0f),




			new Vector4(0.0f, 0.0f, 1.0f, 1.0f),
			new Vector4(0.0f, 0.0f, 1.0f, 1.0f),
			new Vector4(0.0f, 0.0f, 1.0f, 1.0f),

			new Vector4(0.0f, 0.0f, 1.0f, 1.0f),
			new Vector4(0.0f, 0.0f, 1.0f, 1.0f),
			new Vector4(0.0f, 0.0f, 1.0f, 1.0f),

			new Vector4(0.8f, 0.8f, 0.8f, 1.0f),
			new Vector4(0.8f, 0.8f, 0.8f, 1.0f),
			new Vector4(0.8f, 0.8f, 0.8f, 1.0f),

			new Vector4(0.8f, 0.8f, 0.8f, 1.0f),
			new Vector4(0.8f, 0.8f, 0.8f, 1.0f),
			new Vector4(0.8f, 0.8f, 0.8f, 1.0f),

			new Vector4(0.0f, 1.0f, 0.0f, 1.0f),
			new Vector4(0.0f, 1.0f, 0.0f, 1.0f),
			new Vector4(0.0f, 1.0f, 0.0f, 1.0f),

			new Vector4(0.0f, 1.0f, 0.0f, 1.0f),
			new Vector4(0.0f, 1.0f, 0.0f, 1.0f),
			new Vector4(0.0f, 1.0f, 0.0f, 1.0f),

			new Vector4(0.5f, 0.5f, 0.0f, 1.0f),
			new Vector4(0.5f, 0.5f, 0.0f, 1.0f),
			new Vector4(0.5f, 0.5f, 0.0f, 1.0f),

			new Vector4(0.5f, 0.5f, 0.0f, 1.0f),
			new Vector4(0.5f, 0.5f, 0.0f, 1.0f),
			new Vector4(0.5f, 0.5f, 0.0f, 1.0f),

			new Vector4(1.0f, 0.0f, 0.0f, 1.0f),
			new Vector4(1.0f, 0.0f, 0.0f, 1.0f),
			new Vector4(1.0f, 0.0f, 0.0f, 1.0f),

			new Vector4(1.0f, 0.0f, 0.0f, 1.0f),
			new Vector4(1.0f, 0.0f, 0.0f, 1.0f),
			new Vector4(1.0f, 0.0f, 0.0f, 1.0f),

			new Vector4(0.0f, 1.0f, 1.0f, 1.0f),
			new Vector4(0.0f, 1.0f, 1.0f, 1.0f),
			new Vector4(0.0f, 1.0f, 1.0f, 1.0f),

			new Vector4(0.0f, 1.0f, 1.0f, 1.0f),
			new Vector4(0.0f, 1.0f, 1.0f, 1.0f),
			new Vector4(0.0f, 1.0f, 1.0f, 1.0f),

		};

		private int vertexBufferObject;
		private int vao;

		void InitializeVertexBuffer()
		{
			GL.GenBuffers(1, out vertexBufferObject);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer,
				(IntPtr)(vertexData.Length * Vector4.SizeInBytes),
				vertexData,
				BufferUsageHint.StaticDraw);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
		}

		private void init()
		{
			InitializeProgram();
			InitializeVertexBuffer();

			GL.GenVertexArrays(1, out vao);
			GL.BindVertexArray(vao);

			GL.Enable(EnableCap.CullFace);
			GL.CullFace(CullFaceMode.Back);
			GL.FrontFace(FrontFaceDirection.Cw);
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit);

			GL.UseProgram(theProgram);

			GL.Uniform2(offsetUniform, 0.5f, 0.5f);

			int colordata = (vertexData.Length * Vector4.SizeInBytes) / 2;
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
			GL.EnableVertexAttribArray(0);
			GL.EnableVertexAttribArray(1);
			GL.VertexAttribPointer(0, 4, VertexAttribPointerType.Float, false, 0, 0);
			GL.VertexAttribPointer(1, 4, VertexAttribPointerType.Float, false, 0, colordata);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 36);

			GL.DisableVertexAttribArray(0);
			GL.DisableVertexAttribArray(1);
			GL.UseProgram(0);

			SwapBuffers();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			GL.Viewport(0, 0, Width, Height);
		}

		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					Exit();
					break;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			KeyDown += Keyboard_KeyDown;

			init();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Run();
			}
		}
	}
}
