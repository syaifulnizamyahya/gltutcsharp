﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2004%20Objects%20at%20Rest/data/ManualPerspective.vert?at=default

#version 330

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;

smooth out vec4 theColor;

uniform vec2 offset;
uniform float zNear;
uniform float zFar;
uniform float frustumScale;

void main()
{
	vec4 cameraPos = position + vec4(offset.x, offset.y, 0.0f, 0.0f);
	vec4 clipPos;
	
	clipPos.xy = cameraPos.xy * frustumScale;
	
	clipPos.z = cameraPos.z * (zNear + zFar) / (zNear - zFar);
	clipPos.z += 2 * zNear * zFar / (zNear - zFar);
	
	clipPos.w = -cameraPos.z;
	
	gl_Position = clipPos;
	theColor = color;	
}