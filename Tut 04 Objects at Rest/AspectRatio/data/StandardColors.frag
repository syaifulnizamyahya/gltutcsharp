﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2004%20Objects%20at%20Rest/data/StandardColors.frag?at=default

#version 330

smooth in vec4 theColor;

out vec4 outputColor;

void main()
{
	outputColor = theColor;
}
