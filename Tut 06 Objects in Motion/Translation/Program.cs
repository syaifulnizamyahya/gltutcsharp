﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// http://www.arcsynthesis.org/gltut/Positioning/Tut06%20Translation.html
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2006%20Objects%20in%20Motion/Translation.cpp?at=default

using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using framework;

namespace Translation
{
	internal class Program : GameWindow
	{
		private int theProgram;

		private int modelToCameraMatrixUnif;
		private int cameraToClipMatrixUnif;

		private Matrix4 cameraToClipMatrix = Matrix4.Zero;

		static float CalcFrustumScale(float fFovDeg)
		{
			const float degToRad = 3.14159f * 2.0f / 360.0f;
			float fFovRad = fFovDeg * degToRad;
			return (float) (1.0f / Math.Tan(fFovRad / 2.0f));
		}

		private readonly float fFrustumScale = CalcFrustumScale(45.0f);

		private void InitializeProgram()
		{
			var shaderList = new List<int>();

			shaderList.Add(Framework.LoadShader(ShaderType.VertexShader, @"data\PosColorLocalTransform.vert"));
			shaderList.Add(Framework.LoadShader(ShaderType.FragmentShader, @"data\ColorPassthrough.frag"));

			theProgram = Framework.CreateProgram(shaderList);

			modelToCameraMatrixUnif = GL.GetUniformLocation(theProgram, "modelToCameraMatrix");
			cameraToClipMatrixUnif = GL.GetUniformLocation(theProgram, "cameraToClipMatrix");

			float fzNear = 1.0f; float fzFar = 45.0f;

			cameraToClipMatrix.M11 = fFrustumScale;
			cameraToClipMatrix.M22 = fFrustumScale;
			cameraToClipMatrix.M33 = (fzFar + fzNear) / (fzNear - fzFar);
			cameraToClipMatrix.M34 = -1.0f;
			cameraToClipMatrix.M43 = (2 * fzFar * fzNear) / (fzNear - fzFar);

			GL.UseProgram(theProgram);
			GL.UniformMatrix4(cameraToClipMatrixUnif, 1, false, ref cameraToClipMatrix.Row0.X);
			GL.UseProgram(0);
		}

		private int numberOfVertices = 8;

		private static readonly Vector4 GREEN_COLOR = new Vector4(0.75f, 0.75f, 1.0f, 1.0f);
		private static readonly Vector4 BLUE_COLOR = new Vector4(0.0f, 0.5f, 0.0f, 1.0f);
		private static readonly Vector4 RED_COLOR = new Vector4(1.0f, 0.0f, 0.0f, 1.0f);
		private static readonly Vector4 GREY_COLOR = new Vector4(0.8f, 0.8f, 0.8f, 1.0f);
		private static readonly Vector4 BROWN_COLOR = new Vector4(0.5f, 0.5f, 0.0f, 1.0f);

		private static readonly Vector3[] vertexData =
		{
			new Vector3(+1.0f, +1.0f, +1.0f),
			new Vector3(-1.0f, -1.0f, +1.0f),
			new Vector3(-1.0f, +1.0f, -1.0f),
			new Vector3(+1.0f, -1.0f, -1.0f),

			new Vector3(-1.0f, -1.0f, -1.0f),
			new Vector3(+1.0f, +1.0f, -1.0f),
			new Vector3(+1.0f, -1.0f, +1.0f),
			new Vector3(-1.0f, +1.0f, +1.0f),
		};

		private static readonly Vector4[] vertexDataColors =
		{
			GREEN_COLOR,
			BLUE_COLOR,
			RED_COLOR,
			BROWN_COLOR,

			GREEN_COLOR,
			BLUE_COLOR,
			RED_COLOR,
			BROWN_COLOR,
		};

		private short[] indexData =
		{
			0, 1, 2,
			1, 0, 3,
			2, 3, 0,
			3, 2, 1,

			5, 4, 6,
			4, 5, 7,
			7, 6, 4,
			6, 7, 5,
		};

		private int vertexBufferObject;
		private int indexBufferObject;
		private int vao;

		void InitializeVertexBuffer()
		{
			GL.GenBuffers(1, out vertexBufferObject);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer,
				(IntPtr)((vertexData.Length * Vector3.SizeInBytes) +
				(vertexDataColors.Length * Vector4.SizeInBytes)),
				vertexData,
				BufferUsageHint.StaticDraw);
			GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * Vector3.SizeInBytes),
				(IntPtr)(vertexDataColors.Length * Vector4.SizeInBytes),
				vertexDataColors);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

			GL.GenBuffers(1, out indexBufferObject);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferObject);
			GL.BufferData(BufferTarget.ElementArrayBuffer,
				(IntPtr)(indexData.Length * Vector4.SizeInBytes),
				indexData,
				BufferUsageHint.StaticDraw);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
		}

		Vector3 StationaryOffset(float fElapsedTime)
		{
			return new Vector3(0.0f, 0.0f, -20.0f);
		}

		Vector3 OvalOffset(float fElapsedTime)
		{
			const float fLoopDuration = 3.0f;
			const float fScale = 3.14159f*2.0f/fLoopDuration;

			float fCurrTimeThroughLoop = fElapsedTime%fLoopDuration;

			return new Vector3((float) Math.Cos(fCurrTimeThroughLoop*fScale)*4.0f,
				(float)Math.Sin(fCurrTimeThroughLoop*fScale)*6.0f,
				-20.0f);
		}

		Vector3 BottomCircleOffset(float fElapsedTime)
		{
			const float fLoopDuration = 12.0f;
			const float fScale = 3.14159f*2.0f/fLoopDuration;

			float fCurrTimeThrouhLoop = fElapsedTime%fLoopDuration;

			return new Vector3((float)Math.Cos(fCurrTimeThrouhLoop*fScale)*5.0f,
				-3.5f, 
				(float)Math.Sin(fCurrTimeThrouhLoop*fScale)*5.0f - 20.0f);
		}

		struct Instance
		{
			public delegate Vector3 OffsetFunc(float f);

			OffsetFunc CalcOffset;

			Matrix4 ConstructMatrix(float fElapsedTime)
			{
				Matrix4 theMat = new Matrix4.Identity();
 
				theMat.Column3 = new Vector4(CalcOffset(fElapsedTime), 1.0f);
 
				return theMat;
			}
		};

		private Instance inst = new Instance();
		
		Instance[] g_instanceList = 
		{
			StationaryOffset,
			OvalOffset,
			BottomCircleOffset,
		};

		private void init()
		{
			InitializeProgram();
			InitializeVertexBuffer();

			GL.GenVertexArrays(1, out vao);
			GL.BindVertexArray(vao);

			int colorDataOffset = Vector3.SizeInBytes * numberOfVertices;
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
			GL.EnableVertexAttribArray(0);
			GL.EnableVertexAttribArray(1);
			GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
			GL.VertexAttribPointer(1, 4, VertexAttribPointerType.Float, false, 0, colorDataOffset);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferObject);

			GL.BindVertexArray(0);

			GL.Enable(EnableCap.CullFace);
			GL.CullFace(CullFaceMode.Back);
			GL.FrontFace(FrontFaceDirection.Cw);

			GL.Enable(EnableCap.DepthTest);
			GL.DepthMask(true);
			GL.DepthFunc(DepthFunction.Less);
			GL.DepthRange(0.0f, 1.0f);
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			GL.UseProgram(theProgram);

			GL.BindVertexArray(vao);

			GL.Uniform3(offsetUniform, 0.0f, 0.0f, 0.5f);
			GL.DrawElements((BeginMode)PrimitiveType.Triangles, indexData.Length, DrawElementsType.UnsignedShort, 0);

			GL.Uniform3(offsetUniform, 0.0f, 0.0f, -1.0f);
			GL.DrawElementsBaseVertex(PrimitiveType.Triangles, indexData.Length, DrawElementsType.UnsignedShort, (IntPtr)0, numberOfVertices / 2);

			GL.BindVertexArray(0);
			GL.UseProgram(0);

			SwapBuffers();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			perspectiveMatrix.M11 = fFrustumScale / (Width / (float)Height);
			perspectiveMatrix.M22 = fFrustumScale;

			GL.UseProgram(theProgram);
			GL.UniformMatrix4(perspectiveMatrixUnif, 1, false, ref perspectiveMatrix.Row0.X);
			GL.UseProgram(0);

			GL.Viewport(0, 0, Width, Height);
		}

		bool bDepthClampingActive = false;

		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					Exit();
					break;

				case Key.Space:
					if (bDepthClampingActive)
					{
						GL.Disable(EnableCap.DepthClamp);
					}
					else
					{
						GL.Enable(EnableCap.DepthClamp);
					}

					bDepthClampingActive = !bDepthClampingActive;
					break;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			KeyDown += Keyboard_KeyDown;

			init();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Run();
			}
		}
	}
}
