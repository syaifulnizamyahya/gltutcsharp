﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// http://www.arcsynthesis.org/gltut/Positioning/Tutorial%2003.html#d0e2827
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2003%20OpenGLs%20Moving%20Triangle/cpuPositionOffset.cpp?at=default

using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using framework;

namespace cpuPositionOffset
{
	class Program : GameWindow
	{
		private int theProgram;

		void InitializeProgram()
		{
			var shaderList = new List<int>();

			shaderList.Add(Framework.LoadShader(ShaderType.VertexShader, @"data\standard.vert"));
			shaderList.Add(Framework.LoadShader(ShaderType.FragmentShader, @"data\standard.frag"));

			theProgram = Framework.CreateProgram(shaderList);
		}

		readonly Vector4[] vertexPositions =
		{
			new Vector4(0.25f, 0.25f, 0.0f, 1.0f),
			new Vector4(0.25f, -0.25f, 0.0f, 1.0f),
			new Vector4(-0.25f, -0.25f, 0.0f, 1.0f),
		};

		private int positionBufferObject;
		private int vao;

		void InitializeVertexBuffer()
		{
			GL.GenBuffers(1, out positionBufferObject);
			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer,
				(IntPtr)(vertexPositions.Length * Vector4.SizeInBytes),
				vertexPositions,
				BufferUsageHint.StreamDraw);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
		}

		private void init()
		{
			InitializeProgram();
			InitializeVertexBuffer();

			GL.GenVertexArrays(1, out vao);
			GL.BindVertexArray(vao);
		}

		private Stopwatch stopwatch;

		void ComputePositionOffsets(out float fXOffset, out float fYOffset)
		{
			const float fLoopDuration = 5.0f;
			const float fScale = 3.14159f * 2.0f / fLoopDuration;

			var fElapsedTime = (float)stopwatch.Elapsed.TotalMilliseconds / 1000.0f;

			float fCurrTimeThroughLoop = fElapsedTime % fLoopDuration;

			fXOffset = (float)Math.Cos(fCurrTimeThroughLoop * fScale) * 0.5f;
			fYOffset = (float)Math.Sin(fCurrTimeThroughLoop * fScale) * 0.5f;
		}

		void AdjustVertexData(float fXOffset, float fYOffset)
		{
			var fNewData = vertexPositions.DeepClone();

			for (int iVertex = 0; iVertex < vertexPositions.Length; iVertex++)
			{
				fNewData[iVertex].X += fXOffset;
				fNewData[iVertex].Y += fYOffset;
			}

			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)0, (IntPtr)(vertexPositions.Length * Vector4.SizeInBytes), ref fNewData[0]);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
		}
		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			float fXOffset = 0.0f, fYOffset = 0.0f;
			ComputePositionOffsets(out fXOffset, out fYOffset);
			AdjustVertexData(fXOffset, fYOffset);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit);

			GL.UseProgram(theProgram);

			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.EnableVertexAttribArray(0);
			GL.VertexAttribPointer(0, 4, VertexAttribPointerType.Float, false, 0, 0);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			GL.DisableVertexAttribArray(0);
			GL.UseProgram(0);

			SwapBuffers();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			GL.Viewport(0, 0, Width, Height);
		}

		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					Exit();
					break;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			KeyDown += Keyboard_KeyDown;

			stopwatch = new Stopwatch();
			stopwatch.Start();

			init();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Run();
			}
		}
	}
}
