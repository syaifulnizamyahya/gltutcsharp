﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// http://www.arcsynthesis.org/gltut/Positioning/Tut03%20Multiple%20Shaders.html
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2003%20OpenGLs%20Moving%20Triangle/fragChangeColor.cpp?at=default

using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using framework;

namespace fragChangeColor
{
	class Program : GameWindow
	{
		private int theProgram;
		private int elapsedTimeUniform;

		void InitializeProgram()
		{
			var shaderList = new List<int>();

			shaderList.Add(Framework.LoadShader(ShaderType.VertexShader, @"data\calcOffset.vert"));
			shaderList.Add(Framework.LoadShader(ShaderType.FragmentShader, @"data\calcColor.frag"));

			theProgram = Framework.CreateProgram(shaderList);

			elapsedTimeUniform = GL.GetUniformLocation(theProgram, "time");

			int loopDurationUnf = GL.GetUniformLocation(theProgram, "loopDuration");
			int fragLoopDurUnf = GL.GetUniformLocation(theProgram, "fragLoopDuration");

			GL.UseProgram(theProgram);
			GL.Uniform1(loopDurationUnf, 5.0f);
			GL.Uniform1(fragLoopDurUnf, 10.0f);
			GL.UseProgram(0);
		}

		readonly Vector4[] vertexPositions =
		{
			new Vector4(0.25f, 0.25f, 0.0f, 1.0f),
			new Vector4(0.25f, -0.25f, 0.0f, 1.0f),
			new Vector4(-0.25f, -0.25f, 0.0f, 1.0f),
		};

		private int positionBufferObject;
		private int vao;

		void InitializeVertexBuffer()
		{
			GL.GenBuffers(1, out positionBufferObject);
			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer,
				(IntPtr)(vertexPositions.Length * Vector4.SizeInBytes),
				vertexPositions,
				BufferUsageHint.StreamDraw);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
		}

		private void init()
		{
			InitializeProgram();
			InitializeVertexBuffer();

			GL.GenVertexArrays(1, out vao);
			GL.BindVertexArray(vao);
		}

		private Stopwatch stopwatch;

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit);

			GL.UseProgram(theProgram);

			GL.Uniform1(elapsedTimeUniform, (float)stopwatch.Elapsed.TotalMilliseconds / 1000.0f);

			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.EnableVertexAttribArray(0);
			GL.VertexAttribPointer(0, 4, VertexAttribPointerType.Float, false, 0, 0);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			GL.DisableVertexAttribArray(0);
			GL.UseProgram(0);

			SwapBuffers();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			GL.Viewport(0, 0, Width, Height);
		}

		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					Exit();
					break;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			KeyDown += Keyboard_KeyDown;

			stopwatch = new Stopwatch();
			stopwatch.Start();

			init();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Run();
			}
		}
	}
}
