﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// Modified C# port of Arcsynthesis Modern OpenGL tutorial
// http://www.arcsynthesis.org/gltut/Basics/Tutorial%2001.html
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2001%20Hello%20Triangle/tut1.cpp?at=default

using System;
using System.Diagnostics;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;

namespace tut1.alt
{
	class Program : GameWindow
	{
		int program;
		int positionBufferObject;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			// Initialize keyboard
			Keyboard.KeyDown += Keyboard_KeyDown;

			// Initialize vertex 
			var vertexPositions = new[]
			{
				new Vector4(0.75f, 0.75f, 0.0f, 1.0f),
				new Vector4(0.75f, -0.75f, 0.0f, 1.0f),
				new Vector4(-0.75f, -0.75f, 0.0f, 1.0f),
			};

			// Initialize vertex shader
			int vertexShader;

			vertexShader = GL.CreateShader(ShaderType.VertexShader);
			using (var streamReader = new StreamReader("triangle.vert"))
			{
				GL.ShaderSource(vertexShader, streamReader.ReadToEnd());
			}
			GL.CompileShader(vertexShader);
			Debug.WriteLine(GL.GetShaderInfoLog(vertexShader));

			// Initialize fragment shader
			int fragmentShader;

			fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
			using (var streamReader = new StreamReader("triangle.frag"))
			{
				GL.ShaderSource(fragmentShader, streamReader.ReadToEnd());
			}
			GL.CompileShader(fragmentShader);
			Debug.WriteLine(GL.GetShaderInfoLog(fragmentShader));


			// Initialize program
			program = GL.CreateProgram();

			GL.AttachShader(program, vertexShader);
			GL.AttachShader(program, fragmentShader);

			GL.LinkProgram(program);

			Debug.WriteLine(GL.GetProgramInfoLog(program));

			GL.DetachShader(program, vertexShader);
			GL.DetachShader(program, fragmentShader);

			GL.DeleteShader(vertexShader);
			GL.DeleteShader(fragmentShader);

			// Initialize vertex buffer
			GL.GenBuffers(1, out positionBufferObject);
			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer,
				(IntPtr)(vertexPositions.Length * Vector4.SizeInBytes),
				vertexPositions,
				BufferUsageHint.StaticDraw);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

			// Initialize vertex array
			int vao;
			GL.GenVertexArrays(1, out vao);
			GL.BindVertexArray(vao);
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			GL.Viewport(0, 0, Width, Height);
		}

		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			base.OnUpdateFrame(e);
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit);

			GL.UseProgram(program);

			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.EnableVertexAttribArray(0);
			GL.VertexAttribPointer(0, 4, VertexAttribPointerType.Float, false, 0, 0);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			GL.DisableVertexAttribArray(0);
			GL.UseProgram(0);

			SwapBuffers();
		}

		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					Exit();
					break;
			}
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Run();
			}
		}
	}
}
