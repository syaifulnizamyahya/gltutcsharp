﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// http://www.arcsynthesis.org/gltut/Basics/Tutorial%2001.html
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2001%20Hello%20Triangle/tut1.cpp?at=default

using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;

namespace tut1
{
	class Program : GameWindow
	{
		int CreateShader(ShaderType eShaderType, string strShaderFile)
		{
			int shader = GL.CreateShader(eShaderType);
			GL.ShaderSource(shader, strShaderFile);
			GL.CompileShader(shader);

			int status;
			GL.GetShader(shader, ShaderParameter.CompileStatus, out status);
			if (status == 0)
			{
				int infoLogLength;
				GL.GetShader(shader, ShaderParameter.InfoLogLength, out infoLogLength);

				string strInfoLog;
				GL.GetShaderInfoLog(shader, out strInfoLog);

				string strShaderType;
				switch (eShaderType)
				{
					case ShaderType.FragmentShader:
						strShaderType = "fragment";
						break;
					case ShaderType.VertexShader:
						strShaderType = "vertex";
						break;
					case ShaderType.GeometryShader:
						strShaderType = "geometry";
						break;
					default:
						throw new ArgumentOutOfRangeException("eShaderType");
				}

				Debug.WriteLine("Compile failure in " + strShaderType + " shader:\n" + strInfoLog, "Error");
			}

			return shader;
		}

		int CreateProgram(List<int> shaderList)
		{
			int program = GL.CreateProgram();

			foreach (int shader in shaderList)
			{
				GL.AttachShader(program, shader);
			}

			GL.LinkProgram(program);

			int status;
			GL.GetProgram(program, GetProgramParameterName.LinkStatus, out status);
			if (status == 0)
			{
				int infoLogLength;
				GL.GetProgram(program, GetProgramParameterName.InfoLogLength, out infoLogLength);

				string strInfoLog;
				GL.GetProgramInfoLog(program, out strInfoLog);
				Debug.WriteLine("Linker failure: " + strInfoLog, "Error");
				throw new Exception("Linker failure: " + strInfoLog);
			}

			foreach (int shader in shaderList)
			{
				GL.DetachShader(program, shader);
			}

			return program;
		}

		int theProgram;

		private const string strVertexShader = @"#version 330
			layout(location = 0) in vec4 position;
			void main()
			{
			   gl_Position = position;
			}";

		private const string strFragmentShader = @"#version 330
			out vec4 outputColor;
			void main()
			{
			   outputColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
			}";

		void InitializeProgram()
		{
			var shaderList = new List<int>();

			shaderList.Add(CreateShader(ShaderType.VertexShader, strVertexShader));
			shaderList.Add(CreateShader(ShaderType.FragmentShader, strFragmentShader));

			theProgram = CreateProgram(shaderList);

			foreach (int shader in shaderList)
			{
				GL.DeleteShader(shader);
			}
		}

		readonly Vector4[] vertexPositions =
		{
			new Vector4(0.75f, 0.75f, 0.0f, 1.0f),
			new Vector4(0.75f, -0.75f, 0.0f, 1.0f),
			new Vector4(-0.75f, -0.75f, 0.0f, 1.0f),
		};

		int positionBufferObject;
		private int vao;

		void InitializeVertexBuffer()
		{
			GL.GenBuffers(1, out positionBufferObject);
			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer,
				(IntPtr)(vertexPositions.Length * Vector4.SizeInBytes),
				vertexPositions,
				BufferUsageHint.StaticDraw);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
		}

		private void init()
		{
			InitializeProgram();
			InitializeVertexBuffer();

			GL.GenVertexArrays(1, out vao);
			GL.BindVertexArray(vao);
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit);

			GL.UseProgram(theProgram);

			GL.BindBuffer(BufferTarget.ArrayBuffer, positionBufferObject);
			GL.EnableVertexAttribArray(0);
			GL.VertexAttribPointer(0, 4, VertexAttribPointerType.Float, false, 0, 0);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			GL.DisableVertexAttribArray(0);
			GL.UseProgram(0);

			SwapBuffers();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			GL.Viewport(0, 0, Width, Height);
		}

		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					Exit();
					break;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			KeyDown += Keyboard_KeyDown;

			init();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Run();
			}
		}
	}
}
