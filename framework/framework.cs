﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// Modified C# port of Arcsynthesis Modern OpenGL tutorial
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/framework/framework.cpp?at=default

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using OpenTK.Graphics.OpenGL4;
using System.IO;

namespace framework
{
	public static class Framework
	{
		public static int LoadShader(ShaderType eShaderType, string strShaderFilename)
		{
			if (!File.Exists(strShaderFilename))
			{
				Debug.WriteLine("Could not find the file " + strShaderFilename, "Error");
				throw new Exception("Could not find the file " + strShaderFilename);
			}

			int shader = GL.CreateShader(eShaderType);
			using (var streamReader = new StreamReader(strShaderFilename))
			{
				GL.ShaderSource(shader, streamReader.ReadToEnd());
			}

			GL.CompileShader(shader);

			int status;
			GL.GetShader(shader, ShaderParameter.CompileStatus, out status);
			if (status == 0)
			{
				int infoLogLength;
				GL.GetShader(shader, ShaderParameter.InfoLogLength, out infoLogLength);

				string strInfoLog;
				GL.GetShaderInfoLog(shader, out strInfoLog);

				string strShaderType;
				switch (eShaderType)
				{
					case ShaderType.FragmentShader:
						strShaderType = "fragment";
						break;
					case ShaderType.VertexShader:
						strShaderType = "vertex";
						break;
					case ShaderType.GeometryShader:
						strShaderType = "geometry";
						break;
					default:
						throw new ArgumentOutOfRangeException("eShaderType");
				}

				Debug.WriteLine("Compile failure in " + strShaderType + " shader:\n" + strInfoLog, "Error");
				throw new Exception("Compile failure in " + strShaderType + " shader:\n" + strInfoLog);
			}

			return shader;
		}

		public static int CreateProgram(List<int> shaderList)
		{
			int program = GL.CreateProgram();

			foreach (int shader in shaderList)
			{
				GL.AttachShader(program, shader);
			}

			GL.LinkProgram(program);

			int status;
			GL.GetProgram(program, GetProgramParameterName.LinkStatus, out status);
			if (status == 0)
			{
				int infoLogLength;
				GL.GetProgram(program, GetProgramParameterName.InfoLogLength, out infoLogLength);

				string strInfoLog;
				GL.GetProgramInfoLog(program, out strInfoLog);
				Debug.WriteLine("Linker failure: " + strInfoLog, "Error");
				throw new Exception("Linker failure: " + strInfoLog);
			}

			foreach (int shader in shaderList)
			{
				GL.DetachShader(program, shader);
			}

			return program;
		}

		/// <summary>
		/// Creates a deep clone of an object using serialization.
		/// </summary>
		/// <typeparam name="T">The type to be cloned/copied.</typeparam>
		/// <param name="o">The object to be cloned.</param>
		public static T DeepClone<T>(this T o)
		{
			using (var stream = new MemoryStream())
			{
				var formatter = new BinaryFormatter();
				formatter.Serialize(stream, o);
				stream.Position = 0;
				return (T)formatter.Deserialize(stream);
			}
		}
	}
}
