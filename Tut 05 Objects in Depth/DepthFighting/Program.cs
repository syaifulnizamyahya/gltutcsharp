﻿// THIS TUTORIAL IS INCOMPLETE.

// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// http://www.arcsynthesis.org/gltut/Positioning/Tut05%20Boundaries%20and%20Clipping.html
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2005%20Objects%20in%20Depth/VertexClipping.cpp?at=default

using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using framework;

namespace DepthFighting
{
	class Program : GameWindow
	{
		private int theProgram;

		private int offsetUniform;
		private int perspectiveMatrixUnif;

		private Matrix4 perspectiveMatrix;
		private const float fFrustumScale = 1.0f;

		private void InitializeProgram()
		{
			var shaderList = new List<int>();

			shaderList.Add(Framework.LoadShader(ShaderType.VertexShader, @"data\Standard.vert"));
			shaderList.Add(Framework.LoadShader(ShaderType.FragmentShader, @"data\Standard.frag"));

			theProgram = Framework.CreateProgram(shaderList);

			offsetUniform = GL.GetUniformLocation(theProgram, "offset");

			perspectiveMatrixUnif = GL.GetUniformLocation(theProgram, "perspectiveMatrix");

			float fzNear = 1.0f;
			float fzFar = 100000.0f;

			perspectiveMatrix = Matrix4.Zero;

			perspectiveMatrix.M11 = fFrustumScale;
			perspectiveMatrix.M22 = fFrustumScale;
			perspectiveMatrix.M33 = (fzFar + fzNear) / (fzNear - fzFar);
			perspectiveMatrix.M43 = (2 * fzFar * fzNear) / (fzNear - fzFar);
			perspectiveMatrix.M34 = -1.0f;

			GL.UseProgram(theProgram);
			GL.UniformMatrix4(perspectiveMatrixUnif, 1, false, ref perspectiveMatrix.Row0.X);
			GL.UseProgram(0);
		}

		private int numberOfVertices = 8;

		private static readonly Vector4 GREEN_COLOR = new Vector4(0.75f, 0.75f, 1.0f, 1.0f);
		private static readonly Vector4 BLUE_COLOR = new Vector4(0.0f, 0.5f, 0.0f, 1.0f);
		private static readonly Vector4 RED_COLOR = new Vector4(1.0f, 0.0f, 0.0f, 1.0f);

		private static float Z_OFFSET = 0.5f;

		private static readonly Vector3[] vertexData =
		{
			// Front face positions
			new Vector3(-400.0f, 400.0f, 0.0f),
			new Vector3(400.0f, 400.0f, 0.0f),
			new Vector3(400.0f, -400.0f, 0.0f),
			new Vector3(-400.0f, -400.0f, 0.0f),

			//Rear face positions
			new Vector3(-200.0f, 600.0f, -Z_OFFSET),
			new Vector3(600.0f, 600.0f, 0.0f-Z_OFFSET),
			new Vector3(600.0f, -200.0f, 0.0f-Z_OFFSET),
			new Vector3(-200.0f, -200.0f, -Z_OFFSET),

		};

		private static readonly Vector4[] vertexDataColors =
		{
			//Front face colors
			GREEN_COLOR,
			GREEN_COLOR,
			GREEN_COLOR,
			GREEN_COLOR,

			//Rear face colors
			RED_COLOR,
			RED_COLOR,
			RED_COLOR,
			RED_COLOR,

		};

		private short[] indexData =
		{
			0, 1, 3,
			1, 2, 3,

			4, 5, 7,
			5, 6, 7,
		};

		private int vertexBufferObject;
		private int indexBufferObject;
		private int vao;

		void InitializeVertexBuffer()
		{
			GL.GenBuffers(1, out vertexBufferObject);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer,
				(IntPtr)((vertexData.Length * Vector3.SizeInBytes) +
				(vertexDataColors.Length * Vector4.SizeInBytes)),
				vertexData,
				BufferUsageHint.StaticDraw);
			GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * Vector3.SizeInBytes),
				(IntPtr)(vertexDataColors.Length * Vector4.SizeInBytes),
				vertexDataColors);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

			GL.GenBuffers(1, out indexBufferObject);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferObject);
			GL.BufferData(BufferTarget.ElementArrayBuffer,
				(IntPtr)(indexData.Length * Vector4.SizeInBytes),
				indexData,
				BufferUsageHint.StaticDraw);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
		}

		private void init()
		{
			InitializeProgram();
			InitializeVertexBuffer();

			GL.GenVertexArrays(1, out vao);
			GL.BindVertexArray(vao);

			int colorDataOffset = Vector3.SizeInBytes * numberOfVertices;
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
			GL.EnableVertexAttribArray(0);
			GL.EnableVertexAttribArray(1);
			GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
			GL.VertexAttribPointer(1, 4, VertexAttribPointerType.Float, false, 0, colorDataOffset);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferObject);

			GL.BindVertexArray(0);

			GL.Enable(EnableCap.CullFace);
			GL.CullFace(CullFaceMode.Back);
			GL.FrontFace(FrontFaceDirection.Cw);

			GL.Enable(EnableCap.DepthTest);
			GL.DepthMask(true);
			GL.DepthFunc(DepthFunction.Lequal);
			GL.DepthRange(0.0f, 1.0f);
		}

		private float fStart = 2534.0f;
		private float fDelta = 0.0f;

		private Stopwatch stopwatch;

		float CalcZOFfset()
		{
			const float fLoopDuration = 5.0f;
			const float fScale = 3.14159f*2.0f/fLoopDuration;

			var fElapsedTime = (float)stopwatch.Elapsed.TotalMilliseconds / 1000.0f;

			float fCurrTimeThroughLoop = fElapsedTime % fLoopDuration;

			//float fRet = Math.Cos(fCurrTimeThroughLoop*fScale)*500.0f - fStart;
			float fRet = fDelta - fStart;

			return fRet;
		}

		private volatile bool bReadBuffer = false;

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.ClearDepth(1.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			GL.UseProgram(theProgram);
			GL.BindVertexArray(vao);

			float fZOffset = CalcZOFfset();
			GL.Uniform3(offsetUniform, 0.0f, 0.0f, fZOffset);
			GL.DrawElements((BeginMode)PrimitiveType.Triangles, indexData.Length, DrawElementsType.UnsignedShort, 0);

			GL.BindVertexArray(0);
			GL.UseProgram(0);

			SwapBuffers();

			if (bReadBuffer)
			{
				bReadBuffer = false;

				var pBuffer = new int[500*500];
				GL.ReadPixels(0, 0, 500, 500, PixelFormat.DepthStencil, PixelType.UnsignedInt248, pBuffer);

				ErrorCode error = GL.GetError();

				string strOutput;

				var charMap = new Dictionary<int, char>();


			}
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			perspectiveMatrix.M11 = fFrustumScale / (Width / (float)Height);
			perspectiveMatrix.M22 = fFrustumScale;

			GL.UseProgram(theProgram);
			GL.UniformMatrix4(perspectiveMatrixUnif, 1, false, ref perspectiveMatrix.Row0.X);
			GL.UseProgram(0);

			GL.Viewport(0, 0, Width, Height);
		}

		bool bDepthClampingActive = false;

		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					Exit();
					break;

				case Key.Space:
					if (bDepthClampingActive)
					{
						GL.Disable(EnableCap.DepthClamp);
					}
					else
					{
						GL.Enable(EnableCap.DepthClamp);
					}

					bDepthClampingActive = !bDepthClampingActive;
					break;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			KeyDown += Keyboard_KeyDown;

			stopwatch = new Stopwatch();
			stopwatch.Start();

			init();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Width = 500;
				program.Height = 500;
				program.Run();
			}
		}
	}
}
