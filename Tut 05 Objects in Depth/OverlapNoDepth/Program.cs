﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of Arcsynthesis Modern OpenGL tutorial
// http://www.arcsynthesis.org/gltut/Positioning/Tutorial%2005.html#d0e4720
// https://bitbucket.org/alfonse/gltut/src/1d1479cc7027f1e32c5adff748f3b296f1931d84/Tut%2005%20Objects%20in%20Depth/OverlapNoDepth.cpp?at=default 

using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using framework;

namespace OverlapNoDepth
{
	internal class Program : GameWindow
	{
		private int theProgram;

		private int offsetUniform;
		private int perspectiveMatrixUnif;

		private Matrix4 perspectiveMatrix;
		private const float fFrustumScale = 1.0f;

		private void InitializeProgram()
		{
			var shaderList = new List<int>();

			shaderList.Add(Framework.LoadShader(ShaderType.VertexShader, @"data\Standard.vert"));
			shaderList.Add(Framework.LoadShader(ShaderType.FragmentShader, @"data\Standard.frag"));

			theProgram = Framework.CreateProgram(shaderList);

			offsetUniform = GL.GetUniformLocation(theProgram, "offset");

			perspectiveMatrixUnif = GL.GetUniformLocation(theProgram, "perspectiveMatrix");

			float fzNear = 1.0f;
			float fzFar = 3.0f;

			perspectiveMatrix = Matrix4.Zero;

			perspectiveMatrix.M11 = fFrustumScale;
			perspectiveMatrix.M22 = fFrustumScale;
			perspectiveMatrix.M33 = (fzFar + fzNear)/(fzNear - fzFar);
			perspectiveMatrix.M43 = (2*fzFar*fzNear)/(fzNear - fzFar);
			perspectiveMatrix.M34 = -1.0f;

			GL.UseProgram(theProgram);
			GL.UniformMatrix4(perspectiveMatrixUnif, 1, false, ref perspectiveMatrix.Row0.X);
			GL.UseProgram(0);
		}

		private int numberOfVertices = 36;

		private const float RIGHT_EXTENT = 0.8f;
		private const float LEFT_EXTENT = -RIGHT_EXTENT;
		private const float TOP_EXTENT = 0.2f;
		private const float MIDDLE_EXTENT = 0.0f;
		private const float BOTTOM_EXTENT = -TOP_EXTENT;
		private const float FRONT_EXTENT = -1.25f;
		private const float REAR_EXTENT = -1.75f;

		private static readonly Vector4 GREEN_COLOR = new Vector4(0.75f, 0.75f, 1.0f, 1.0f);
		private static readonly Vector4 BLUE_COLOR = new Vector4(0.0f, 0.5f, 0.0f, 1.0f);
		private static readonly Vector4 RED_COLOR = new Vector4(1.0f, 0.0f, 0.0f, 1.0f);
		private static readonly Vector4 GREY_COLOR = new Vector4(0.8f, 0.8f, 0.8f, 1.0f);
		private static readonly Vector4 BROWN_COLOR = new Vector4(0.5f, 0.5f, 0.0f, 1.0f);

		private static readonly Vector3[] vertexData =
		{
			//Object 1 positions
			new Vector3(LEFT_EXTENT, TOP_EXTENT, REAR_EXTENT),
			new Vector3(LEFT_EXTENT, MIDDLE_EXTENT, FRONT_EXTENT),
			new Vector3(RIGHT_EXTENT, MIDDLE_EXTENT, FRONT_EXTENT),
			new Vector3(RIGHT_EXTENT, TOP_EXTENT, REAR_EXTENT),

			new Vector3(LEFT_EXTENT, BOTTOM_EXTENT, REAR_EXTENT),
			new Vector3(LEFT_EXTENT, MIDDLE_EXTENT, FRONT_EXTENT),
			new Vector3(RIGHT_EXTENT, MIDDLE_EXTENT, FRONT_EXTENT),
			new Vector3(RIGHT_EXTENT, BOTTOM_EXTENT, REAR_EXTENT),

			new Vector3(LEFT_EXTENT, TOP_EXTENT, REAR_EXTENT),
			new Vector3(LEFT_EXTENT, MIDDLE_EXTENT, FRONT_EXTENT),
			new Vector3(LEFT_EXTENT, BOTTOM_EXTENT, REAR_EXTENT),

			new Vector3(RIGHT_EXTENT, TOP_EXTENT, REAR_EXTENT),
			new Vector3(RIGHT_EXTENT, MIDDLE_EXTENT, FRONT_EXTENT),
			new Vector3(RIGHT_EXTENT, BOTTOM_EXTENT, REAR_EXTENT),

			new Vector3(LEFT_EXTENT, BOTTOM_EXTENT, REAR_EXTENT),
			new Vector3(LEFT_EXTENT, TOP_EXTENT, REAR_EXTENT),
			new Vector3(RIGHT_EXTENT, TOP_EXTENT, REAR_EXTENT),
			new Vector3(RIGHT_EXTENT, BOTTOM_EXTENT, REAR_EXTENT),

			//Object 2 positions
			new Vector3(TOP_EXTENT, RIGHT_EXTENT, REAR_EXTENT),
			new Vector3(MIDDLE_EXTENT, RIGHT_EXTENT, FRONT_EXTENT),
			new Vector3(MIDDLE_EXTENT, LEFT_EXTENT, FRONT_EXTENT),
			new Vector3(TOP_EXTENT, LEFT_EXTENT, REAR_EXTENT),

			new Vector3(BOTTOM_EXTENT, RIGHT_EXTENT, REAR_EXTENT),
			new Vector3(MIDDLE_EXTENT, RIGHT_EXTENT, FRONT_EXTENT),
			new Vector3(MIDDLE_EXTENT, LEFT_EXTENT, FRONT_EXTENT),
			new Vector3(BOTTOM_EXTENT, LEFT_EXTENT, REAR_EXTENT),

			new Vector3(TOP_EXTENT, RIGHT_EXTENT, REAR_EXTENT),
			new Vector3(MIDDLE_EXTENT, RIGHT_EXTENT, FRONT_EXTENT),
			new Vector3(BOTTOM_EXTENT, RIGHT_EXTENT, REAR_EXTENT),

			new Vector3(TOP_EXTENT, LEFT_EXTENT, REAR_EXTENT),
			new Vector3(MIDDLE_EXTENT, LEFT_EXTENT, FRONT_EXTENT),
			new Vector3(BOTTOM_EXTENT, LEFT_EXTENT, REAR_EXTENT),

			new Vector3(BOTTOM_EXTENT, RIGHT_EXTENT, REAR_EXTENT),
			new Vector3(TOP_EXTENT, RIGHT_EXTENT, REAR_EXTENT),
			new Vector3(TOP_EXTENT, LEFT_EXTENT, REAR_EXTENT),
			new Vector3(BOTTOM_EXTENT, LEFT_EXTENT, REAR_EXTENT),
		};

		private static readonly Vector4[] vertexDataColors =
		{
			//Object 1 colors
			GREEN_COLOR,
			GREEN_COLOR,
			GREEN_COLOR,
			GREEN_COLOR,

			BLUE_COLOR,
			BLUE_COLOR,
			BLUE_COLOR,
			BLUE_COLOR,

			RED_COLOR,
			RED_COLOR,
			RED_COLOR,

			GREY_COLOR,
			GREY_COLOR,
			GREY_COLOR,

			BROWN_COLOR,
			BROWN_COLOR,
			BROWN_COLOR,
			BROWN_COLOR,

			//Object 2 colors
			RED_COLOR,
			RED_COLOR,
			RED_COLOR,
			RED_COLOR,

			BROWN_COLOR,
			BROWN_COLOR,
			BROWN_COLOR,
			BROWN_COLOR,

			BLUE_COLOR,
			BLUE_COLOR,
			BLUE_COLOR,

			GREEN_COLOR,
			GREEN_COLOR,
			GREEN_COLOR,

			GREY_COLOR,
			GREY_COLOR,
			GREY_COLOR,
			GREY_COLOR,
		};

		private short[] indexData =
		{
			0, 2, 1,
			3, 2, 0,

			4, 5, 6,
			6, 7, 4,

			8, 9, 10,
			11, 13, 12,

			14, 16, 15,
			17, 16, 14,
		};

		private int vertexBufferObject;
		private int indexBufferObject;

		private int vaoObject1, vaoObject2;

		void InitializeVertexBuffer()
		{
			GL.GenBuffers(1, out vertexBufferObject);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer,
				(IntPtr)((vertexData.Length * Vector3.SizeInBytes) + 
				(vertexDataColors.Length * Vector4.SizeInBytes)),
				vertexData,
				BufferUsageHint.StaticDraw);
			GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr) (vertexData.Length*Vector3.SizeInBytes),
				(IntPtr) (vertexDataColors.Length*Vector4.SizeInBytes), vertexDataColors);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

			GL.GenBuffers(1, out indexBufferObject);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferObject);
			GL.BufferData(BufferTarget.ElementArrayBuffer, 
				(IntPtr) (indexData.Length*Vector4.SizeInBytes), 
				indexData,
				BufferUsageHint.StaticDraw);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
		}

		void InitializeVertexArrayObjects()
		{
			GL.GenVertexArrays(1, out vaoObject1);
			GL.BindVertexArray(vaoObject1);

			int colorDataOffset = Vector3.SizeInBytes * numberOfVertices;

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferObject);
			GL.EnableVertexAttribArray(0);
			GL.EnableVertexAttribArray(1);
			GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
			GL.VertexAttribPointer(1, 4, VertexAttribPointerType.Float, false, 0, colorDataOffset);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferObject);

			GL.BindVertexArray(0);

			GL.GenVertexArrays(1, out vaoObject2);
			GL.BindVertexArray(vaoObject2);

			int posDataOffset = Vector3.SizeInBytes*(numberOfVertices/2);
			colorDataOffset += Vector4.SizeInBytes*(numberOfVertices/2);

			GL.EnableVertexAttribArray(0);
			GL.EnableVertexAttribArray(1);
			GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, posDataOffset);
			GL.VertexAttribPointer(1, 4, VertexAttribPointerType.Float, false, 0, colorDataOffset);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferObject);

			GL.BindVertexArray(0);
		}

		private void init()
		{
			InitializeProgram();
			InitializeVertexBuffer();
			InitializeVertexArrayObjects();

			GL.Enable(EnableCap.CullFace);
			GL.CullFace(CullFaceMode.Back);
			GL.FrontFace(FrontFaceDirection.Cw);
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit);

			GL.UseProgram(theProgram);

			GL.BindVertexArray(vaoObject1);
			GL.Uniform3(offsetUniform, 0.0f, 0.0f, 0.0f);
			GL.DrawElements((BeginMode) PrimitiveType.Triangles, indexData.Length, DrawElementsType.UnsignedShort, 0);

			GL.BindVertexArray(vaoObject2);
			GL.Uniform3(offsetUniform, 0.0f, 0.0f, -1.0f);
			GL.DrawElements((BeginMode)PrimitiveType.Triangles, indexData.Length, DrawElementsType.UnsignedShort, 0);

			GL.BindVertexArray(0);
			GL.UseProgram(0);

			SwapBuffers();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			perspectiveMatrix.M11 = fFrustumScale / (Width / (float)Height);
			perspectiveMatrix.M22 = fFrustumScale;

			GL.UseProgram(theProgram);
			GL.UniformMatrix4(perspectiveMatrixUnif, 1, false, ref perspectiveMatrix.Row0.X);

			GL.Viewport(0, 0, Width, Height);
		}

		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					Exit();
					break;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			KeyDown += Keyboard_KeyDown;

			init();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Run();
			}
		}
	}
}
